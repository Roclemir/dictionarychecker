/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include "shared.h"

#ifndef CHECKER_H
#define CHECKER_H
#define NUMARGS 2
#define FILEARG 1
#define ZEROINDEXOFFSET -1

/**
 * @file checker.h contains any declarations and data structures required for 
 * checker.c
 **/

/**
 * forwards declaration of the word_list as we need to know about it in thi 
 * file to declare some pointers
 **/
struct word_list;

/**
 * the heart of our system - contains a pointer to the word_list and a pointer
 * to the dictionary file name for saving later
 **/
struct checker_system
{
        /**
         * a pointer to our word list
         **/
        struct word_list * list;
        /**
         * the name of our dictionary file for saving later
         **/
        const char * word_file;
};

/**
 * Handles RTM value recieved when at top level menu already
 **/
void print_rtm_message();
#endif
