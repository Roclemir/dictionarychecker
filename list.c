/***********************************************************************
 * CPT220 - Programming in C
 * Study Period 4 2015 Assignment #3
 * Full Name        : Ryan Couper
 * Student Number   : s3575400
 * Start up code provided by Paul Miller
 ***********************************************************************/
#include "list.h"

/**
 * @file list.c contains implementation of functions for managing the word lists
 * used by the program.
 **/

/**
 * Initialises the list to a safe state.
 **/
BOOLEAN init_list( struct word_list ** list )
{
        struct word_list * mem_test;
        mem_test = ( struct word_list * )malloc( 
                        sizeof( struct word_list ) );
        if( !mem_test )
        {
                perror( "Failed to assign memory for word_list!" );
                return FALSE;
        }
        *list = mem_test;
        memset( *list, 0, sizeof( struct word_list ) );
        return TRUE;
}
/**
 * Add an item to the list in sorted order
 **/
BOOLEAN list_add( struct word_list * list, char * word, size_t size )
{
        struct list_node * node, * curr = list->head, * prev = NULL;
        struct word * newword;

        /* Check if the word exists in the list already */
        if( word_exists( list, word ) )
        {
                printf( "Word exists in dictionary\n" );
                return FALSE;
        }

        /* Create memory space for the word struct */
        newword = ( struct word * )malloc( sizeof( struct word ) );
        if( !newword )
        {
                perror( "Failed to allocate memory for new word!" );
                return FALSE;
        }
        memset( newword, 0, sizeof( struct word ) );
        /* Create space for the new word */
        newword->word = ( char * )malloc( size );

        /* Strip newline char from the word if it exists */
        strip_nl( word );

        strcpy( newword->word, word );

        /* Create memory space for node */
        node = ( struct list_node * )malloc( sizeof( struct list_node ) );
        if( !node )
        {
                perror( "Failed to allocate memory for new list node!" );
                return FALSE;
        }

        /* Set node values */
        node->data = newword;
        node->next = NULL;

        /* Handle adding first element to the list */
        if( !(list->head) )
        {
                list->head = node;
                ++list->num_words;
                return TRUE;
        }

        /* Find insertion point in list */
        while( curr && word_cmp( curr->data->word, node->data->word ) < 0 )
        {
                prev = curr;
                curr = curr->next;
        }

        /* Handle adding to end of the list */
        if( !curr )
        {
                prev->next = node;
                ++list->num_words;
        } else if( !prev ) { /* Handle insertion at start of list */
                node->next = list->head;
                list->head = node;
                ++list->num_words;
        } else { /* Handle insertion in the middle of the list */
                node->next = curr;
                prev->next = node;
                ++list->num_words;
        }

        return TRUE;
}

/**
 * Remove an item from the list
 **/
BOOLEAN list_del( struct word_list * list, const char * word )
{
        struct list_node * curr = list->head, * prev = NULL;

        /* Handle empty list */
        if( !( list->head ) )
        {
                puts( "Empty list" );
                return FALSE;
        }

        while( curr->next && word_cmp( curr->data->word, word ) != 0 )
        {
                prev = curr;
                curr = curr->next;
        }

        /* Handle only 1 element in the list */
        if( !( list->head->next ) )
        {
                node_free( list->head );
                list->head = NULL;
        } else if( !( curr->next ) ) {
                /* Deletion at end of the list */
                prev->next = NULL;
                node_free( curr );
        } else if ( !prev ) {
                /* Deletion at the start of the list */
                list->head = list->head->next;
                node_free( curr );
        } else {
                /* Deletion in the middle of the list */
                prev->next = curr->next;
                node_free( curr );
        }

        return TRUE;
}
/**
 * Free the entire list
 **/
void list_free( struct word_list * list )
{
        struct list_node * curr = list->head, * prev = NULL;

        while( curr )
        {
                prev = curr;
                curr = curr->next;
                node_free( prev );
        }
        free( list );
}
/**
 * Free a node 
 **/
void node_free( struct list_node * node )
{
        struct_word_free( node->data );
        free( node );
}

/**
 * Free the memory reserved for each word struct
 **/
void struct_word_free( struct word * word_data )
{
        word_free( word_data->word );
        free( word_data );
}

/**
 * Free the memory for the actually word
 **/
void word_free( char * word )
{
        free( word );
}

#ifdef WITH_LEVENSHTEIN
/**
 * @param first the first string to compare
 * @param first_len the number of characters in the first string to compare
 * @param second the second string to compare
 * @param second_len the number of characters in the second string to compare
 * @return the minimum number of edits required to turn the first string into
 * the second string 
 **/
int levenshtein(const char * first, int first_len, 
                const char * second, int second_len)
{
        int shorten_both, shorten_first, shorten_second;

        /* if either string is shorter than the threshold, difference is 
           inserting all chars from the other
           */
        if (first_len < THRESHOLD) return second_len;
        if (second_len < THRESHOLD) return first_len;
        printf("%s:%s\n", first, second);
        /* if the difference in the length of the strings is greater than the 
         * threshold, they are not similar
         */
        if(abs(strlen(first) - strlen(second)) > THRESHOLD)
        {
                return INT_MAX;
        }

        /* if last letters are the same, the difference is whatever is
         * required to edit the rest of the strings
         */
        if (first[first_len] == second[second_len])
                return levenshtein(first, first_len - 1, second, 
                                second_len - 1);

        /* else try:
         *      changing last letter of first to that of second; or
         *      remove last letter of first; or
         *      remove last letter of second,
         * any of which is 1 edit plus editing the rest of the strings
         */
        shorten_both = levenshtein(first, first_len - 1, second, 
                        second_len - 1);
        shorten_first = levenshtein(first, first_len,     second, 
                        second_len - 1);
        shorten_second = levenshtein(first, first_len - 1, second, 
                        second_len    );
        return (shorten_both <= shorten_first) ? 
                ((shorten_both <=  shorten_second) ? 
                 shorten_both + 1 : shorten_second + 1)
                : shorten_first + 1;
}
/**
 * Uses the levenshtein function to find the two closest words to the word
 * entered in the second paramter when compared to the list given by the
 * first parameter
 **/
void find_nearest_words( struct word_list * list, char * word )
{
        struct list_node * curr = list->head;
        char * first, * second;
        unsigned long fst_score = 0, sec_score = 0, ls_score = 0;

        while( curr != NULL )
        {
                ls_score = levenshtein( curr->data->word,
                                        word_size( curr->data->word ),
                                        word,
                                        word_size( word ) );
                if( fst_score == 0 )
                {
                        first = curr->data->word;
                        fst_score = ls_score;
                } else if( sec_score == 0 ) {
                        second = curr->data->word;
                        sec_score = ls_score;
                } else {
                        if( ls_score < fst_score )
                        {
                                fst_score = ls_score;
                                first = curr->data->word;
                        } else if( ls_score < sec_score ) {
                                sec_score = ls_score;
                                second = curr->data->word;
                        }
                }
                curr = curr->next;
        }

        /* Display the two closest words */
        printf( "Did not find %s in the dictionary\n", word );
        printf( "The two closest words are %s and %s/n/n", first, second );
}
#endif
