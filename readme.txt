########################################################################
# CPT220 - Programming in C
# Study Period 4 2015 Assignment #3
# Full Name        : Ryan Couper
# Student Number   : s3575400
# Start up code provided by Paul Miller
########################################################################

Outline any issues that your marker needs to be aware of when marking
your assignment.

I have implemented the function with levenshtein depending on how
you call the makefile.
To get the program without the levenshtein function type:
usercommandline]$ make
To activate the function with the levenshtein algorithm, type:
usercommandline]$ make checker_WL
NOTE: I have never had a successful check of a file with the
levenshtein function, this is due to the slow way it works out the
distance between words. The use of the levenshtein function is the
only difference between the programs.

Adding the compiler flag -DDEBUG will add a few extra things:
1) there will be an 8th menu option for printing the list of
   dictionary words. Useful for testing system initialisation.
2) If the spell check option is selected, after loading the text
   file into a new list, it prints out the list of words in the
   text file.

In options.c there is a function for comparing the words from the 
text file and the dictionary. One of the requested requirements is 
to print out all the non-matched words from the text file. I could
have implemented this by printing them out as they were found, but
that solution didn't seem elegant as it required various printf() 
statements before and after the loop and made it difficult to handle
punctuation. 
I could also have counted the unmatched words and created an array
of that size and then looped through it again to find the words and 
add it to the array, but this would have taken a long time,especially
for large files. Instead I chose to create a third list and store
the unmatched words in there, then cycle over this list and print
them later. This seemed the more elegant solution since all the
functions were already in place to achieve this.

The #define for the menu text probably wasn't necessary however I
couldn't decide on an implementation method and found myself 
rewriting the same menu text 3 times and I still wasn't happy, so to
avoid rewriting it again I #define'd it.

In the functions located in options.c, upon the user hitting ctrl-d
or entering on empty string, a message is printed and the function
returns TRUE. It returns TRUE because the user wanting to go back
is not an error, and these functions only return FALSE when an 
error has occurred.
