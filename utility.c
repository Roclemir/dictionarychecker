/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#include "utility.h"
#include "list.h"
/**
 * @file utility.c contains implementations of helper functions and i/o functions
 * for the program
 **/

/**
 * clear the input buffer when the user has entered more data than allowed.
 * You should only call this when you have detected leftover input in the 
 * input buffer
 **/
static void read_rest_of_line(void)
{
	int ch;
	while(ch = getc(stdin), ch != '\n' && ch != EOF)
		;
	clearerr(stdin);
}

/**
 * @param checker the datastructure that holds all the data for the
 * system
 **/
BOOLEAN system_init(struct checker_system * checker)
{
        memsafe( checker, sizeof( struct checker_system ) );
	return TRUE;
}

/**
 * @param checker the datastructure that maintains all the information 
 * about the system
 * @param filename the filename to load the dictionary from
 **/
BOOLEAN load_data(struct checker_system* checker, const char * filename)
{
        FILE * fp;
        char word[ BUFSIZ ];

        /* Gracefully open the dictionary file */
        if( !( fp = fopen( filename, "rb" ) ) )
        {
                perror( "Failed to open file" );
                return FALSE;
        }

        /* Cycle through the list of words in the dictionary file */
        while( fgets( word, BUFSIZ, fp ) )
        {
                if( word_exists( checker->list, word ) )
                {
                        /* Don't add to the list */
                
                } else if( check_word( word ) ) /* check if it's a word */
                {
                        /* convert characters to lowercase */
                        word_to_lower( word );
                        if( !( list_add( checker->list, 
                                      word,
                                      word_size( word ) ) ) )
                        { return FALSE;
                        }

                } else {
                        printf( "This is not a word: %s\n", word );
                }
        }
        fclose( fp );
        return TRUE;
}

/**
 * @param checker the datastructure used to manage the system
 **/
void system_free( struct checker_system* checker )
{
        list_free( checker->list );
} 

/**
 * Takes a pointer to a sequential block of memory and nulls/zeros it
 **/
void memsafe( void * data, size_t size )
{
        memset( data, 0, size );
}

/**
 * Checks to ensure the word is in fact a word and does not contain 
 * anything other that letters
 **/
BOOLEAN check_word( const char * word )
{
        BOOLEAN isword = TRUE; /* innocent until proven guilty */
        unsigned count = 0;
        while( word[ count ] != '\0' && word[ count ] != '\n' && isword )
        {
                if( !( isalpha( word[ count++ ] ) ) )
                {
                        isword = FALSE; /* GUILTY! */
                }
        }
        
        return isword;
}

/**
 * Changes any uppercase letters of a word to lowercase
 **/
void word_to_lower( char * word )
{
        unsigned count = 0;

        while( word[ count ] )
        {
                word[ count ] = tolower( word[ count ] );
                ++count;
        }
}

/**
 * Gets the size of the word
 **/
unsigned word_size( const char * word )
{
        unsigned count = 0;
        while( word[ count++ ] )
                  ;

        return count + 1; /* for '\0' */
}

/**
 * Checks if the word is already in the list. 
 * Returns true if it finds a match.
 **/
BOOLEAN word_exists( struct word_list * list, const char * word )
{
        struct list_node * curr = list->head, * prev = NULL;

        /* Handle empty list */
        if( !(list->head) ) return FALSE;

        /* Handle 1 item in list */
        if( list->head->next == NULL )
        {
                if( word_cmp( word, list->head->data->word ) == 0 )
                        return TRUE;
        }

        /* Cycle through larger list */
        while( curr )
        {
                prev = curr;
                curr = curr->next;
                if( word_cmp( prev->data->word, word ) == 0 ) /* A match */
                        return TRUE;
        }
        return FALSE;
}

/**
 * Compares two words and returns an int indicating there amount of
 * difference
 **/
int word_cmp( const char * first, const char * second )
{
        return strcmp( first, second );
}

/**
 * Checks the user input for buffer overflow and if overflow is detected, 
 * calls the buffer flushing function.
 * This funtion is a pass/fail function, and therefore returns TRUE if no
 * overflow is detected, FALSE if it is detected.
 **/
enum input_result check_buffer( char * input )
{
        if( input[ strlen( input ) - ONE_LETTER ] != '\n' ) /* Ctrl-d */
        {
                printf( "Error: too many characters for input!" );
                read_rest_of_line();
                return FAILURE;
        }
        if( input[ 0 ] == '\n' ) /* User hit enter without any input */
                return RTM;
        return SUCCESS;
}

/** 
 * For getting input from the user
 **/
enum input_result get_input( char * input, size_t strlen )
{
        if( fgets( input, strlen + EXTRACHARS, stdin ) == NULL ) return RTM;
        return check_buffer( input );
}

/** 
 * Message to be displayed in the return to menu event
 **/
void print_rtm( void )
{
        puts( "Returning to main menu" );
}

/**
 * Strips the trailing newline char of a word
 **/
void strip_nl( char * word )
{
        if( word[ strlen( word ) - ONE_LETTER ] == '\n' )
                word[ strlen( word ) - ONE_LETTER ] = '\0';
}

#ifdef DEBUG
void print_list( struct word_list * list )
{
        struct list_node * curr = list->head, * prev = NULL;

        while( curr )
        {
                prev = curr;
                curr = curr->next;
                printf( "%s\n", prev->data->word );
        }
}

/**
 * Wrapper function to allow the above function to show up in the list
 * menu when DEBUG is defined
 **/
BOOLEAN print_menu_opt( struct checker_system * checker )
{
        print_list( checker->list );
        return TRUE;
}
#endif
