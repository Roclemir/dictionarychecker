/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#include <string.h>
#include "shared.h"
#include "options.h"
#include "checker.h"
#include "utility.h"

#ifndef MENU_H
#define MENU_H
/**
 * @file menu.h contains the declaration of datastructures and function 
 * prototypes required for managing the menu system
 **/

/**
 * the length of text for each menu item
 **/
#define MENU_TEXT_LEN 40
/**
 * how many items are in the menu
 **/
#ifdef DEBUG
#define NUM_MENU_ITEMS 8
#else
#define NUM_MENU_ITEMS 7
#endif

/**
 * For helper funtion when displaying the menu
 **/
#define PRINTLINELEN 30

/**
 * Greeting message
 **/
#define WELCOMEMSG "Welcome to the Spell Checker"

/** 
 * Menu item text, stored here for easy change
 * 
 **/
#define MENUTITLE_TXT       "Please select an option"
#define SPCHK_TXT           "1) Spell check a file"
#define ADDWD_TXT           "2) Add a new word to the dictionary"
#define DELWD_TXT           "3) Delete a word from the dictionary"
#define DISPSTATS_TXT       "4) Display word stats"
#define CLRSTATS_TXT        "5) Clear word stats"
#define SAVE_TXT            "6) Save dictionary and exit"
#define QUIT_TXT            "7) Quit without saving"
#ifdef DEBUG
#define PRINT_TXT           "8) Print dictionary"
#endif

/**
 * represents a menu item in our array with a pointer to the function that 
 * implements this job and a string that contains the menu text for this 
 * menu item
 **/
struct menu_item
{
        /** 
	 * a pointer to the function that implements this menu item 
	 **/
        BOOLEAN (*checker_func)(struct checker_system*);
        /** 
	 * the text to be displayed for this menu item 
	 **/
        char text[MENU_TEXT_LEN+1];
};

/**
 * function that initialises each element of the the menu array to:
 * <ul>
 *    <li>contain a pointer to the function that implements that menu item</li>
 *    <li>contain the text to be displayed for that menu item</li>
 * </ul>
 **/
void init_menu(struct menu_item *);

/**
 * function that displays the menu to the user
 **/
void display_menu( struct menu_item * );

/* Prints a basic line of dashes to the terminal */
void print_line( void );

/* For checking user input agains possible menu items */
enum input_result check_menu_input( const char * choice );
#endif
