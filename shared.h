/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#ifndef SHARED_H
#define SHARED_H
/**
 * @file shared.h defines the data structures that need to be shared across all
 * files. This is mainly here so we can avoid recursive includes for commonly 
 * used structures
 **/

/**
 * Input number base for strtol
 **/
#define BASE 10
/**
 * definition of the BOOLEAN datatype
 **/
typedef enum
{
        /**
         * value for FALSE (0)
         **/
        FALSE,
        /** 
         * value for TRUE (1)
         **/
        TRUE
} BOOLEAN;

/**
 * To enhance readability for menu item selection - the O is for OPTION
 **/
enum
{
        SPELLCHECK_O,
        ADDWORD_O,
        DELWORD_O,
        STATS_O,
        CLEARSTATS_O,
        SAVEEXIT_O,
        QUIT_O
#ifdef DEBUG
        , PRINT_O
#endif
};
#endif
