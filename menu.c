/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#include "menu.h"

/**
 * @file menu.c contains the function implementations for managing the menu
 * system
 **/

/**
 * @param menu the array of menu items passed in to be initialized
 **/
void init_menu(struct menu_item * menu)
{
        /* Declare and init two arrays, one with strings for names, and
         * the other to hold the functions */
        /* Iterate over the array, initialising each element as we go */
        char menu_opt[ NUM_MENU_ITEMS ][ MENU_TEXT_LEN +1 ];
        BOOLEAN ( *opt_func[ NUM_MENU_ITEMS ] )( struct checker_system * );
        unsigned opt_num;
        
        /* Create text for menu items */
        strcpy( menu_opt[ SPELLCHECK_O ], SPCHK_TXT );
        strcpy( menu_opt[ ADDWORD_O ],    ADDWD_TXT );
        strcpy( menu_opt[ DELWORD_O ],    DELWD_TXT );
        strcpy( menu_opt[ STATS_O ],      DISPSTATS_TXT );
        strcpy( menu_opt[ CLEARSTATS_O ], CLRSTATS_TXT );
        strcpy( menu_opt[ SAVEEXIT_O ],   SAVE_TXT );
        strcpy( menu_opt[ QUIT_O ],       QUIT_TXT );
#ifdef DEBUG
        strcpy( menu_opt[ PRINT_O ],      PRINT_TXT );
#endif

        /* Get function pointers for menu items */
        opt_func[ SPELLCHECK_O ] =      spell_check;
        opt_func[ ADDWORD_O ] =         add_word;
        opt_func[ DELWORD_O ] =         del_word;
        opt_func[ STATS_O ] =           stats_report;
        opt_func[ CLEARSTATS_O ] =      clear_stats;
        opt_func[ SAVEEXIT_O ] =        save_and_exit;
        opt_func[ QUIT_O ] =            quit;
#ifdef DEBUG
        opt_func[ PRINT_O ] =           print_menu_opt;
#endif

        /* Add the options to the array of menu items */
        for( opt_num = 0; opt_num < NUM_MENU_ITEMS; ++opt_num )
        {
                strcpy( menu[ opt_num ].text, menu_opt[ opt_num ] );
                menu[ opt_num ].checker_func = opt_func[ opt_num ];
        }

}

/**
 * @param menu the menu array to display the contents of 
 **/
void display_menu(struct menu_item * menu)
{
        unsigned count;
        puts( WELCOMEMSG );
        print_line();
        puts( MENUTITLE_TXT );
        print_line();
        for( count = 0; count < NUM_MENU_ITEMS; ++count )
        {
                printf( "%s\n", menu[ count ].text );
        }
}

/* Helper function for printing the menu */
void print_line( void )
{
        unsigned count;
        for( count = 0; count < PRINTLINELEN; ++count ) printf( "-" );
        puts( "" );
}

/* For checking user input agains possible menu items */
enum input_result check_menu_input( const char * choice )
{
        int user_choice;
        char * end;
        BOOLEAN pass = isdigit( choice[ 0 ] );
        if( pass )
        {
                user_choice = strtol( choice, &end, BASE );
                if( user_choice <= NUM_MENU_ITEMS && user_choice >= 0 )
                {
                        pass = TRUE;
                } else {
                        pass = FALSE;
                }
        }
        if( pass )
        {
                return SUCCESS;
        } else {
                return FAILURE;
        }
}
