/***********************************************************************
 * CPT220 - Programming in C
 * Study Period 4 2015 Assignment #3
 * Full Name        : Ryan Couper
 * Student Number   : s3575400
 * Start up code provided by Paul Miller
 ***********************************************************************/
#include "checker.h"
#include "list.h"
#include "menu.h"
/**
 * @mainpage This is the main html documentation of assignment 3 for
 * CPT220 - Programming in C for study period 4, 2015. You will find
 * here a description of the functions that you need to implement for
 * your assignment.
 *
 * Please note that this should not be your first stop when starting
 * your assignment. Please read the assignment 1 specifications that
 * are available on blackboard before reading this documentation. The
 * purpose of this documentation is to clarify the actual individual
 * requirements.
 *
 * This site presents you with the documentation for each function
 * that you need to implement broken down by the file that they exist
 * in. Please go to the Files tab and click on the file you wish to
 * get more information about and then click on that file. An
 * explanation of each function implemented in that file will be
 * displayed.
 **/

/**
 * @file checker.c
 * contains the main function and any related helper files
 **/

/**
 * the entry point for your program. From here you should initialise 
 * datastructures and load data into the system and then manage the menu
 * loop. Once the user has decided to quit, you should make sure any resources
 * required by the system are released and cleaned up. 
 **/
int main(int argc, char** argv)
{
        /* array of menu items including the text for each menu item and a 
         * pointer to the callback function
         */
        struct menu_item menu[NUM_MENU_ITEMS];
        BOOLEAN quit = FALSE;
        /* holds all the data for our system */
        struct checker_system checker;
        /* holds the user's menu choice */
        char c_choice[ ONE_LETTER + EXTRACHARS ];
        int choice;

        /* For menu input validation */
        enum input_result pass = SUCCESS;

        /* check for the correct number of command line arguments */
        if( NUMARGS != argc )
        {
                fprintf( stderr, "Error: Invalid arguments entered\n"
                                "The correct way to run this program is:\n"
                                "\t./checker <dictfile>\n"
                                "where dictfile is a line separated file "
                                "of words.\n\n" );
                return EXIT_FAILURE;
        }
        /* ensure that the data structures are initialized to a safe, 
         * empty state
         */
        system_init( &checker );
        checker.word_file = argv[ FILEARG ];
        init_list( &( checker.list ) );
        if( !( checker.list ) ) return EXIT_FAILURE;
        checker.list->type = T_DICT;

        /* load the data into the system */
        if( !( load_data( &checker, checker.word_file ) ) )
        {
                fprintf( stderr, "Failed to load dictionary data." );
                return EXIT_FAILURE;
        }

        /* print out the size of our dictionary */
        printf( "%ld words loaded into the dictionary\n", 
                        checker.list->num_words );
        /* initialize the menu data structure */
        init_menu( menu );        
        while( !quit )
        {
                char * end; /* For strtol() */
                display_menu( menu );
                /* get choice from the user */
                puts( "Enter your selection:" );
                pass = get_input( c_choice, ONE_LETTER );
                /* Check user choice is valid */
                if( pass == SUCCESS )
                        pass = check_menu_input( c_choice );

                if( pass == RTM )
                        print_rtm_message();

                /* Turn user's choice into a number */
                choice = strtol( c_choice, &end, BASE ) ZEROINDEXOFFSET;

                /* run the requested checker function */
                if( pass == SUCCESS )
                {
                        if( choice == SAVEEXIT_O || choice == QUIT_O )
                                quit = TRUE;
                                
                        /* Check of the funciton ran successfully */
                        if( menu[ choice ].checker_func( &checker ) )
                        {
                                printf( "\n\"%s\" was executed "
                                        "successfully\n\n",
                                        menu[ choice ].text );
                        } else {
                                printf( "\n\"%s\" failed to execute "
                                        "successfully\n\n",
                                        menu[ choice ].text );
                        }
                } else {
                        puts( "Invalid input." );
                        puts( "Please select one of the menu options." );
                }
        }

        return EXIT_SUCCESS;
}

/**
 * Handles RTM value recieved when at top level menu already
 **/
void print_rtm_message()
{
        puts( "Already in main menu." );
        printf( "To exit choose option %d or %d.\n",
                        SAVEEXIT_O, QUIT_O );
}
