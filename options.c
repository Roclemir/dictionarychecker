/***********************************************************************
 * CPT220 - Programming in C
 * Study Period 4 2015 Assignment #3
 * Full Name        : Ryan Couper
 * Student Number   : s3575400
 * Start up code provided by Paul Miller
 ***********************************************************************/
#include "options.h"

/**
 * @file options.c contains the implementations of all functions for implementing
 * the options as specificied in the assignment specification
 **/

/**
 * @param checker datastructure that holds all data for managing the system
 * @return @ref TRUE when we successfully spellcheck a file and @ref FALSE
 * otherwise
 **/
BOOLEAN spell_check(struct checker_system* checker)
{
        FILE * fp;
        char input[ BUFSIZ ];
        struct word_list * txt_list;
        BOOLEAN fileopen = FALSE;

        /* Initialise list for words of text file */
        init_list( &txt_list );
        txt_list->type = T_CHECK;

        /* Allow the user to enter filename for file to be checked.
         * Loop to allow the user to accdentally make a mistake in
         * the file name
         **/
        while( !fileopen )
        {
                puts( "Enter filename to be spell checked" );
                printf( "file: " );
                if( get_input( input, MAX_WORD_LEN + EXTRACHARS )== RTM )
                {
                        print_rtm();
                        list_free( txt_list );
                        return TRUE;
                }
                input[ strlen( input ) - ONE_LETTER ] = '\0';
                if( !( fp = fopen( input, "rb" ) ) )
                {
                        puts( "File failed to open" );
                        puts( "Choose another filename or hit enter to "
                                        "return to menu" );
                } else {
                        fileopen = TRUE;
                }
        }

        /* load the file into memory */
        if( !load_file( fp, txt_list ) )
        {
                fprintf( stderr, "Error loading text file\n" );
                return FALSE;
        }

        check_words( checker->list, txt_list );

        fclose( fp );
        list_free( txt_list );
        return TRUE;
}

/**
 * Adds inc_by to the matching word in the word list
 **/
void inc_word_count( struct word_list * list, char * word )
{
        struct list_node * curr = list->head;

        /* Find the word in the list */
        while( curr != NULL && word_cmp( curr->data->word, word ) != 0 )
        {
                curr = curr->next;
        }       
        /* Add to the count */
        ++curr->data->count;
}

/**
 * @param checker datastructure that holds all the data for the system
 **/
BOOLEAN add_word(struct checker_system* checker)
{
        char word[ MAX_WORD_LEN + EXTRACHARS ];

        /* Get user input for a word */
        puts( "Please note all words added to the dictionary are converted"
                        " to lower case." );
        puts( "Enter word to add to the dictionary: " );
        if( get_input( word, MAX_WORD_LEN + EXTRACHARS ) == RTM )
        {
                print_rtm();
                return TRUE;
        }

        /* Check if the input is a valid word */
        strip_nl( word );
        if( check_word( word ) == FALSE )
        {
                printf( "Error: %s is not a word\n", word );
                return FALSE;
        }

        /* Add it to the dictionary */
        word_to_lower( word );
        if(  list_add( checker->list, word, word_size( word ) ) )
        {
                printf( "%s successfully added to the list\n", word );
                return TRUE;
        } else {
                printf( "Adding %s was unsuccessful\n", word );
                return FALSE;
        }
}

/**
 * @param checker datastructure that holds all the data for the system
 **/
BOOLEAN del_word(struct checker_system* checker)
{
        char word[ MAX_WORD_LEN + EXTRACHARS ];

        puts( "Enter the word you wish to delete:" );
        if( get_input( word, MAX_WORD_LEN + EXTRACHARS ) == RTM )
        {
                print_rtm();
                return TRUE;
        }
        strip_nl( word );

        /* Check if it is a valid word */
        if( !( check_word( word ) ) )
        {
                printf( "Error: %s is not a word\n", word );
                return FALSE;
        }

        /* Delete it from the list if it exists */
        if( list_del( checker->list, word ) )
        {
                printf( "%s successfully removed from the list\n", word );
                return TRUE;
        } else {
                return FALSE;
        }
}

/**
 * @param checker datastructure that maintains the data for the system
 **/
BOOLEAN stats_report(struct checker_system * checker)
{
        unsigned count;
        struct list_node * curr = checker->list->head;
        char ch, input[ ONE_LETTER + EXTRACHARS ];
        enum input_result success = FALSE;
        BOOLEAN printed = FALSE;
        /* Bi-boolean to decide when to print and when we're done with the
         * loop */
        BOOLEAN done = FALSE, print = FALSE; 

        printf( " This program allows you to output basic stats about all\n"
                        " words that start with the same letter. What letter would\n"
                        " you like a report on: " );
        success = get_input( input, ONE_LETTER + EXTRACHARS );
        if( success == RTM )
        {
                print_rtm();
                return TRUE;
        } else if( success == FAILURE ) {
                return FAILURE;
        }
        if( !check_word( input ) )
        {
                printf( "'%s' is not a letter\n", input );
                return FALSE;
        }
        word_to_lower( input );
        ch = input[ FIRST_CHAR ];

        while( curr && !done )
        {
                if( print == FALSE 
                    && curr->data->word[ FIRST_CHAR ] == ch
                    && curr->data->count != 0 )
                {
                        print = TRUE;
                        /* Print out pretty title bar */
                        printf( "\n%-40s| %s", "Word", 
                                        " Word Frequency\n " );
                        for( count = 0; count < STATLINELEN; ++count )
                                printf( "-" );
                        puts( "" );
                }
                if( print && curr->data->word[ FIRST_CHAR ] != ch )
                {
                        print = FALSE;
                        done = TRUE;
                }

                if( print && curr->data->count != 0 )
                {
                        printf( "%-40s| %-20d\n",
                                        curr->data->word,
                                        curr->data->count );
                        if( !printed )
                                printed = TRUE;
                }
                if( curr )
                        curr = curr->next;
        }
        if( !printed )
        {
                printf( "There are no words that were found that start "
                                "with the letter '%c'.\n", ch );
        }

        return TRUE;
}

/**
 * @param checker the datastructures that help us manage the overall system
 **/
BOOLEAN clear_stats (struct checker_system * checker)
{
        /* Cycle over the list setting word count to zero */
        struct list_node * curr = checker->list->head;
        unsigned zero = 0;
        while( curr )
        {
                curr->data->count = zero;
                curr = curr->next;
        }
        return TRUE;
}

/**
 * @param checker the datastructures that help us manage the system
 **/
BOOLEAN save_and_exit(struct checker_system* checker)
{
        FILE * fp;
        struct list_node * curr;

        /* Deal with an empty list */
        if( !checker->list->head )
        {
                printf( "List is empty, nothing to save\n" );
                return FALSE;
        }

        /* Gracefully open the file to save to */
        if( !( fp = fopen( checker->word_file, "wb" ) ) )
        {
                perror( "Failed to open file for saving dictionary" );
                return FALSE;
        }

        /* Save each word in the file */
        curr = checker->list->head;
        while( curr )
        {
                fputs( curr->data->word, fp );
                fputs( "\n\0", fp );
                curr = curr->next;
        }

        fclose( fp );

        system_free( checker );
        return TRUE;
}

/**
 * @param checker the datastructures that help us manage the system
 **/
BOOLEAN quit(struct checker_system* checker)
{
        system_free( checker );
        return TRUE;
}

/**
 * Takes a file pointer and a pointer to a list and adds the contents of the
 * file to the list
 **/
BOOLEAN load_file( FILE * fp, struct word_list * txt_list )
{
        char * txt_word = NULL;
        char ch, delims[ NONALPHADELIMS ], line[ BUFSIZ ];
        unsigned count = 0;
        memset( delims, '\0', sizeof( char ) * NONALPHADELIMS );
        memset( line, '\0', BUFSIZ );
        /* Create a list of delimeters for strtok */
        while( ( ch = fgetc( fp ) ) != EOF )
        {
                if( !( isalpha( ch ) ) )
                {
                        if( !( strchr( delims, ch ) ) )
                                delims[ count++ ] = ch;
                }
        }
        /* reset file pointer position */
        if( fseek( fp, 0, SEEK_SET ) )
        {
                perror( "Fatal error with file pointer arithmetic" );
                return FALSE;
        }

        /* Tokenize the file entering each word as we go */
        while( fgets(line, BUFSIZ, fp ) )
        {
                /* First strtok per line read */
                txt_word = strtok( line, delims );
                if( txt_word )
                {
                        word_to_lower( txt_word );
                        if( !word_exists( txt_list, txt_word ) )
                        { 
                                list_add( txt_list, txt_word, 
                                                word_size( txt_word ) );
                        }
                        inc_word_count( txt_list, txt_word );
                }
                /* The rest of the tokens in the line */
                while( ( txt_word = strtok( NULL, delims ) ) )
                {
                        if( txt_word )
                        {
                                word_to_lower( txt_word );
                                if( !word_exists( txt_list, txt_word ) )
                                { 
                                        list_add( txt_list, txt_word, 
                                                        word_size( txt_word ) );
                                }
                                inc_word_count( txt_list, txt_word );
                        }
                }
        }

#ifdef DEBUG
        print_list( txt_list );
#endif
        return TRUE;
}

/**
 * Performs the spell checking of the words from the file
 **/
void check_words( struct word_list * ch_list, struct word_list * txt_list )
{
        struct list_node * ch_curr, * txt_curr;
        struct word_list * not_matched;

        if( !init_list( &not_matched ) )
        {
                fprintf( stderr, "Could not initiate list for unmatched"
                                "words\n" );
                return;
        }
        not_matched->type = T_CHECK;
        ch_curr = ch_list->head;
        txt_curr = txt_list->head;

        /* Loop through words in the list loaded from the file */
        while( txt_curr )
        {
                ch_curr = ch_list->head;
                /* Loop through the dictionary words looking for a match */
                while( ch_curr && strcmp( txt_curr->data->word,
                                        ch_curr->data->word ) != 0 )
                {
                        ch_curr = ch_curr->next;
                }
                /* If there isn't a match, add to not-matched list */
                if( !ch_curr )
                {
#ifdef WITH_LEVENSHTEIN
                        find_nearest_words( ch_list, txt_curr->data->word );
#else
                        list_add( not_matched, 
                                        txt_curr->data->word,
                                        word_size( txt_curr->data->word ) );
#endif
                } else {
                        /* If there's a match, add word count to the dict count */
                        ch_curr->data->count += txt_curr->data->count;
                }
                txt_curr = txt_curr->next;
        }
#ifndef WITH_LEVELSHTEIN 
        /* Print out unmatched words */
        txt_curr = not_matched->head;
        printf( "The following words were in your text file but missing"
                        " from our dictionary: " );
        while( txt_curr )
        {
                /* Print the list of unmatched words, including skipping the
                 * last comma and adding a new line after the last word */
                if( txt_curr->next )
                {
                        printf( "%s, ", txt_curr->data->word );
                } else {
                        printf( "%s\n", txt_curr->data->word );
                }
                txt_curr = txt_curr->next;
        }
#endif

        list_free( not_matched );
}

