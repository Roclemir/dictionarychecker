########################################################################
# CPT220 - Programming in C
# Study Period 4 2015 Assignment #3
# Full Name        : Ryan Couper
# Student Number   : s3575400
# Start up code provided by Paul Miller
########################################################################
SOURCES=checker.c list.c options.c menu.c utility.c
HEADERS=checker.h list.h options.h menu.h utility.h shared.h
README=readme.txt
MAKEFILE=Makefile
WL=-DWITH_LEVENSHTEIN
CFLAGS=-ansi -pedantic -Wall -Wextra -Wshadow -g
TARGETS=checker archive
all:$(TARGETS)

archive:$(USER)-a3.zip
$(USER)-a3.zip:
	zip $@ $(SOURCES) $(HEADERS) $(README) $(MAKEFILE)

checker:checker.o menu.o options.o list.o utility.o

checker.o:checker.c checker.h shared.h
	gcc $(CFLAGS) checker.c -c 

list.o:list.c list.h
	gcc $(CFLAGS) list.c -c

options.o:options.c options.h
	gcc $(CFLAGS) options.c -c

utility.o:utility.c utility.h
	gcc $(CFLAGS) utility.c -c

menu.o:menu.c menu.h
	gcc $(CFLAGS) menu.c -c

checker_WL:checker.wl list.wl options.wl utility.wl menu.wl
	gcc checker.wl list.wl options.wl utility.wl menu.wl -o checker_WL

checker.wl:checker.c checker.h shared.h
	gcc $(CFLAGS) $(WL) checker.c -c -o checker.wl

list.wl:list.c list.h
	gcc $(CFLAGS) $(WL) list.c -c -o list.wl

options.wl:options.c options.h
	gcc $(CFLAGS) $(WL) options.c -c -o options.wl

utility.wl:utility.c utility.h
	gcc $(CFLAGS) $(WL) utility.c -c -o utility.wl

menu.wl:menu.c menu.h
	gcc $(CFLAGS) $(WL) menu.c -c -o menu.wl


.PHONY clean:
	rm checker *.o $(USER)-a3.zip checker_WL *.wl
