/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2015 Assignment #3
* Full Name        : Ryan Couper
* Student Number   : s3575400
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "shared.h"
#include "checker.h"

#ifndef UTILITY_H
#define UTILITY_H
/**
 * @file utility.h contains the data structure definitions and prototypes 
 * required for helper functions and i/o for the program
 **/
#define MAX_WORD_LEN 30
#define EXTRACHARS 2
#define ONE_LETTER 1

/**
 * enumeration that defines the possible outcomes of an i/o operation
 **/
enum input_result
{
        /**
         * it was successful!
         **/
        SUCCESS,
        /**
         * it failed
         **/
        FAILURE,
        /**
         * the user pressed enter or ctrl-d on a new line
         **/
        RTM
};

/**
 * Forwards declaration for system_free for freeing the memory
 **/

/**
 * initialise the system to a known safe state
 **/
BOOLEAN system_init(struct checker_system *);

/**
 * load the specified dictionary file into a linked list
 **/
BOOLEAN load_data(struct checker_system*, const char *);

/**
 * free all data allocated for the system
 **/
void system_free(struct checker_system*);

/**
 * for initialising things to a safe state
 **/
void memsafe( void * data, size_t size );
/**
 * Checks to ensure the word is in fact a word and does not contain 
 * anything other that letters
 **/
BOOLEAN check_word( const char * word );

/**
 * Changes any uppercase letters of a word to lowercase
 **/
void word_to_lower( char * word );

/**
 * Returns the size of a word
 **/
unsigned word_size( const char * word );

/**
 * Checks if the word already exists in the list
 **/
BOOLEAN word_exists( struct word_list * list, const char * word );

/**
 * Compares two words returning and integer indicating the difference 
 * between the two
 **/
int word_cmp( const char * first, const char * second );

/**
 * Checks the user input for buffer overflow and if overflow is detected, 
 * calls the buffer flushing function.
 **/
enum input_result check_buffer( char * input );

/** 
 * For getting input from the user
 **/
enum input_result get_input( char * input, size_t strlen );

/**
 * Message printed when user hits ctrl-d or enters empty string
 **/
void print_rtm( void );

/**
 * Strips the newline character from a word if it exists
 **/
void strip_nl( char * word );
#ifdef DEBUG
void print_list( struct word_list * list );
/**
 * Wrapper function to allow the above function to show up in the list
 * menu when DEBUG is defined
 **/
BOOLEAN print_menu_opt( struct checker_system * );
#endif /* DEBUG */

#endif
